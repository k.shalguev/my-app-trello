import React, { useEffect } from 'react';
import styled from 'styled-components';
import { CrossSvg } from '../../assets/svg';

interface IPopUp {
  closeButton?: boolean;
  closePopUp: () => void;
  children: React.ReactNode | React.ReactChild;
}

export function PopUp({ closeButton, closePopUp, children }:IPopUp) {
  if (closeButton) {
    useEffect(() => {
      window.addEventListener('keydown', keyUpHandler);

      return ():void => window.removeEventListener('keydown', keyUpHandler);
    }, []);
  }

  const keyUpHandler = (event: KeyboardEvent) => {
    if (event.code === 'Escape') {
      closePopUp();
    }
  };

  return (
    <Root>
      <Content>
        {closeButton
        && (
          <Button
            onClick={closePopUp}
          >
            <CrossSvg />
          </Button>
        )}
        {children}
      </Content>
    </Root>
  );
}

PopUp.defaultProps = {
  closeButton: false,
};

const Root = styled.div`
      height: 100%;
      width: 100%;
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1;
      background-color: rgba(0, 0, 0, .5);
      transition: 0.3s;
      display: flex;
      align-items: center;
      justify-content: center;
    `;

const Content = styled.div`
      padding: 24px;
      position: relative;
      background-color: white;
      box-sizing: border-box;
      border-radius: 4px;
    `;

const Button = styled.button`
      padding: 0;
      position: absolute;
      right: 4px;
      top: 4px;
      background-color: transparent;
      border: none;
      outline: none;
      cursor: pointer;
    `;
