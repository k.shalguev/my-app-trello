import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import { PopUp } from '../PopUp';
import { ICardData } from '../../types/types';
import { CardComments, CardDescription, CardHeader } from '../CardDetails/Components';

interface ICard {
  data: ICardData;
  columnTitle: string,
  deleteCard: (id: string) => void;
  changeData: (id: string, data: ICardData) => void;
}

export function Card({
  data, columnTitle, deleteCard, changeData,
}: ICard) {
  const [cardData, setCardData] = useState(data);
  const [isOpenPopUp, setIsOpenPopUp] = useState(false);

  useEffect(() => {
    changeData(data.cardId, cardData);
  }, [cardData]);

  const closePopUp = () => {
    setIsOpenPopUp(false);
  };

  const changeCardTitle = (text: string) => {
    setCardData((prevState) => ({ ...prevState, title: text }));
  };

  const changeCardDescription = (text: string) => {
    setCardData((prevState) => ({ ...prevState, description: text }));
  };

  const addCommentsData = (text: string) => {
    const newComments = [...cardData.comments,
      { text, id: uuidv4() }];
    setCardData((prevState) => ({ ...prevState, comments: newComments }));
  };

  const deleteCommentsData = (id: string) => {
    const newComments = cardData.comments.filter((item) => item.id !== id);
    setCardData((prevState) => ({ ...prevState, comments: newComments }));
  };

  return (
    <Root>
      <MinCard onClick={() => setIsOpenPopUp(true)}>
        <h4>{data.title}</h4>
        <Button
          onClick={() => deleteCard(data.cardId)}
        >
          ✖
        </Button>
        <NumberComments>
          {data.comments.length}
        </NumberComments>
      </MinCard>
      {
        isOpenPopUp
        && (
        <PopUp
          closePopUp={closePopUp}
          closeButton
        >
          <CardHeader
            data={data}
            columnTitle={columnTitle}
            changeData={(value: string) => {
              changeCardTitle(value);
            }}
          />
          <CardDescription
            text={data.description}
            changeData={(value: string) => {
              changeCardDescription(value);
            }}
          />
          <CardComments
            comments={cardData.comments}
            addCommentsData={addCommentsData}
            deleteCommentsData={deleteCommentsData}
          />
        </PopUp>
        )
      }
    </Root>
  );
}

const Root = styled.div`
      display: flex;
      flex-direction: column;
    `;

const MinCard = styled.div`
      min-height: 34px;
      width: 100%;
      margin: 8px 0;
      padding: 8px;
      font-size: 14px;
      display: flex;
      align-items: center;
      border: 1px solid black;
      box-sizing: border-box;
      cursor: pointer;
      overflow: hidden;
      position: relative;
    `;

const Button = styled.div`
      position: absolute;
      right: 8px;
      top: 0;
      color: red;
      background-color: transparent;
      border: none;
      outline: none;
    `;

const NumberComments = styled.div`
      position: absolute;
      right: 8px;
      bottom: 8px;
    `;
