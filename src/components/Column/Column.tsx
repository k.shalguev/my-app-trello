import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import { Card } from '../Card';
import { ICardData, IData } from '../../types/types';

interface IColumn {
  data: IData;
}

export function Column({ data }: IColumn) {
  const [title, setTitle] = useState(data.columnTitle);
  const [cardTitle, setCardTitle] = useState('');
  const [columnData, setColumnData] = useState<IData>(data);

  useEffect(() => {
    const localStorageData = localStorage.getItem(columnData.id);
    if (localStorageData) {
      const parseStorage = JSON.parse(localStorageData);
      setTitle(parseStorage.columnTitle);
      setColumnData(parseStorage);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(
      columnData.id,
      JSON.stringify({ ...columnData }),
    );
  }, [columnData]);

  const handleColumnTitleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setTitle(event.target.value);
  };

  const handleCardTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCardTitle(event.target.value);
  };

  const changeTitle = () => {
    if (title !== columnData.columnTitle) {
      setColumnData((prevState) => ({ ...prevState, columnTitle: title }));
    }
    return null;
  };

  const changeCardData = (id: string, cardData: ICardData) => {
    const newCards: ICardData[] = [];
    columnData.cards.map((item: ICardData) => {
      if (id !== item.cardId) {
        newCards.push(item);
      }
      if (id === item.cardId) {
        newCards.push(cardData);
      }
      return null;
    });
    setColumnData((prevState):IData => (
      {
        ...prevState,
        cards: newCards,
      }));
  };

  const addCard = () => {
    if (cardTitle) {
      const newCards = [{
        cardId: uuidv4(), title: cardTitle, description: '', comments: [],
      }];
      setColumnData((prevState):IData => (
        {
          ...prevState,
          cards: [...prevState.cards, ...newCards],
        }));
      setCardTitle('');
    }
  };

  const deleteCard = (id: string) => {
    const newCards = columnData.cards.filter((item) => item.cardId !== id);
    setColumnData((prevState):IData => (
      {
        ...prevState,
        cards: newCards,
      }));
  };

  return (
    <Root>
      <Title
        value={title}
        onBlur={changeTitle}
        onChange={handleColumnTitleChange}
      />
      {
        (columnData.cards.length > 0)
        && columnData.cards.map((item) => (
          <Card
            data={item}
            key={item.cardId}
            columnTitle={columnData.columnTitle}
            deleteCard={(id:string) => deleteCard(id)}
            changeData={(id: string, cardData: ICardData) => changeCardData(id, cardData)}
          />
        ))
      }
      <Input
        type="text"
        value={cardTitle}
        onChange={handleCardTitleChange}
      />
      <Button
        onClick={addCard}
      >
        Добавить карточку
      </Button>
    </Root>
  );
}

const Root = styled.div`
      margin: 16px;
      padding: 16px;
      min-width: 310px;
      max-width: 310px;
      border: 1px solid black;
      border-radius: 4px;
      box-sizing: border-box;
    `;

const Title = styled.textarea`
      height: 34px;
      width: 100%;
      margin: 0;
      font-size: 24px;
      color: #61dafb;
      box-sizing: border-box;
      resize: none;
      outline: none;
      border: none;
    `;

const Button = styled.button`
      width: 100%;
      margin: 8px 0;
      font-size: 14px;
      cursor: pointer;
    `;

const Input = styled.input`
      width: 100%;
      box-sizing: border-box;
    `;
