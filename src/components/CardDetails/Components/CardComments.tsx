import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { ICommentData } from '../../../types/types';

interface IDescription {
  comments: ICommentData[];
  addCommentsData: (value: string) => void;
  deleteCommentsData: (value: string) => void;
}

export function CardComments({ comments, addCommentsData, deleteCommentsData }: IDescription) {
  const [commentTextarea, setCommentTextarea] = useState('');
  const [userName, setUserName] = useState('');

  useEffect(() => {
    const name = localStorage.getItem('username');
    if (name) {
      setUserName(name);
    }
  }, []);

  const handleChangeComment = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setCommentTextarea(event.target.value);
  };

  const addComment = () => {
    if (commentTextarea) {
      addCommentsData(commentTextarea);
      setCommentTextarea('');
    }
  };

  const deleteComment = (event: React.MouseEvent) => {
    const id = event.currentTarget.getAttribute('data-id');
    if (id) deleteCommentsData(id);
  };

  return (
    <Root>
      <Title>Комментарии</Title>
      {
        (comments.length > 0)
        && comments.map((item) => (
          <Comment
            key={item.id}
          >
            {item.text}
            <UserName>
              Автор:
              {' '}
              <b>{userName}</b>
            </UserName>
            <Button
              data-id={item.id}
              onClick={deleteComment}
            >
              ✖
            </Button>
          </Comment>
        ))
      }
      <div>
        <Textarea
          value={commentTextarea}
          onChange={handleChangeComment}
        />
        <button
          type="button"
          onClick={addComment}
        >
          Добавить комментарий
        </button>
      </div>
    </Root>
  );
}

const Root = styled.div`
      display: flex;
      flex-direction: column;
    `;

const Title = styled.h4`
      margin: 32px 0 8px 0;
    `;

const Comment = styled.div`
      position: relative;
      border: 1px solid cadetblue;
      padding: 8px;
      margin: 4px 0;
    `;

const Textarea = styled.textarea`
      width: 100%;
      height: 56px;
      margin-top: 24px;
      box-sizing: border-box;
      resize: none;
    `;

const Button = styled.div`
      position: absolute;
      right: 8px;
      top: 0;
      color: red;
      background-color: transparent;
      border: none;
      outline: none;
      cursor: pointer;
    `;

const UserName = styled.div`
      background-color: darkseagreen;
      margin-top: 8px;
      padding: 4px;
    `;
