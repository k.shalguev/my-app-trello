import React, { useState } from 'react';
import styled from 'styled-components';

interface IDescription {
  text: string;
  changeData: (value: string) => void;
}

export function CardDescription({ text, changeData }: IDescription) {
  const [description, setDescription] = useState(text);
  const [isEditFieldActive, setIsEditFieldActive] = useState(false);

  const handleChangeDescription = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setDescription(event.target.value);
  };

  const showEditField = () => {
    setIsEditFieldActive(true);
  };

  const saveData = () => {
    changeData(description);
    setIsEditFieldActive(false);
  };

  return (
    <Root>
      {
        !isEditFieldActive
        && (
          <Description
            onClick={showEditField}
          >
            {text || 'Добавить описание'}
          </Description>
        )
      }
      {
        isEditFieldActive
        && (
        <>
          <Textarea
            value={description}
            placeholder="Добавить описание карточки"
            onChange={handleChangeDescription}
          >
            {text}
          </Textarea>
          <Button
            type="button"
            onClick={saveData}
          >
            Сохранить
          </Button>
        </>
        )
      }
    </Root>
  );
}

const Root = styled.div`
      max-width: 330px;
      display: flex;
      flex-direction: column;
      overflow: hidden;
    `;

const Textarea = styled.textarea`
      width: 100%;
      height: 76px;
      outline: none;
      resize: none;
      box-sizing: border-box;
    `;

const Button = styled.button`
      width: 100%;
      margin: 16px 0;
    `;

const Description = styled.p`
      width: 100%;
      margin: 0;
      padding: 12px;
      background-color: #091e420a;
      box-sizing: border-box;
      border-radius: 4px;
      cursor: pointer;
    `;
