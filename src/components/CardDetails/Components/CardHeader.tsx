import React, { useState } from 'react';
import styled from 'styled-components';
import { ICardData } from '../../../types/types';

interface ICardHeader {
  data: ICardData;
  columnTitle: string;
  changeData: (value:string) => void;
}

export function CardHeader({ data, columnTitle, changeData }: ICardHeader) {
  const [cardTitle, setCardTitle] = useState(data.title);

  const handleChangeCardTitle = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setCardTitle(event.target.value);
  };

  const changeCardTitle = () => {
    changeData(cardTitle);
  };

  return (
    <Root>
      <Title
        value={cardTitle}
        onChange={handleChangeCardTitle}
        onBlur={changeCardTitle}
      >
        {data.title}
      </Title>
      <p>
        В колонке
        {' '}
        {columnTitle}
      </p>
    </Root>
  );
}

const Root = styled.div`
      display: flex;
      flex-direction: column;
    `;

const Title = styled.textarea`
      height: 34px;
      width: 100%;
      margin: 0;
      font-size: 24px;
      color: #61dafb;
      box-sizing: border-box;
      resize: none;
      outline: none;
      border: none;
    `;
