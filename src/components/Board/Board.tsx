import React, { useState, useEffect, useReducer } from 'react';
import styled from 'styled-components';
import { PopUp } from '../PopUp';
import { Column } from '../Column';
import { reducer, initialState } from '../../state';

export function Board() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isOpenPopUp, setIsOpenPopUp] = useState(true);

  useEffect(() => {
    const localUserName = localStorage.getItem('username');
    if (localUserName) {
      setIsOpenPopUp(false);
      dispatch({ type: 'nameEntry', payload: localUserName });
    }
  }, []);

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch({ type: 'nameEntry', payload: e.target.value });
  };

  const writeName = () => {
    if (!localStorage.getItem('username')) {
      localStorage.setItem('username', state.username);
      setIsOpenPopUp(false);
    }
  };

  const closePopUp = () => {
    setIsOpenPopUp(false);
  };

  return (
    <Root>
      {
        isOpenPopUp
        && (
        <PopUp
          closePopUp={closePopUp}
        >
          <input
            type="text"
            placeholder="Введите имя"
            value={state.username}
            onChange={handleNameChange}
          />
          <Button
            type="button"
            onClick={writeName}
          >
            Сохранить
          </Button>
        </PopUp>
        )
      }
      {
        state.columns.map((item) => (
          <Column
            key={item.id}
            data={item}
          />
        ))
      }
    </Root>
  );
}

const Root = styled.section`
      padding: 32px;
      display: flex;
      align-items: flex-start;
    `;

const Button = styled.button`
      margin: 0 0 0 8px;
    `;
