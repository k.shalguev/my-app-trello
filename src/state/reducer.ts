export const initialState: IDataState = {
  username: '',
  columns: [
    {
      id: 'columnID-1',
      columnTitle: 'ToDo',
      cards: [],
    },
    {
      id: 'columnID-2',
      columnTitle: 'In progress',
      cards: [],
    },
    {
      id: 'columnID-3',
      columnTitle: 'Testing',
      cards: [],
    },
    {
      id: 'columnID-4',
      columnTitle: 'Done',
      cards: [],
    },
  ],
};

interface IDataState {
  username: string;
  columns: {
    id: string;
    columnTitle: string;
    cards: {
      title: string;
      cardId: string;
      description: string;
      comments: {text: string; id: string}[];
    }[]
  }[];
}

interface IAction {
  type: string;
  payload: string;
}

export function reducer(state: IDataState, action: IAction) {
  switch (action.type) {
    case 'nameEntry':
      return {
        ...state,
        username: action.payload,
      };
    default:
      return state;
  }
}
