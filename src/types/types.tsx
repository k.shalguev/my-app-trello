export interface ICommentData {
  text: string;
  id: string;
}

export interface ICardData {
  title: string;
  cardId: string;
  description: string;
  comments: ICommentData[];
}

export interface IData {
  id: string;
  columnTitle: string;
  cards: ICardData[];
}
